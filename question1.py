class Node:
  def __init__(self,data):
    self.data = data
    self.next = None

class Linkedlist:
  def __init__(self):
    self.head = None 
  
  def insert_at_begining(self,data):
    new_node = Node(data)
    new_node.next = self.head 
    self.head = new_node
  
  def insert_at_end(self,data):
    new_node = Node(data)
    temp = self.head 
    while temp.next:
      temp = temp.next  
    temp.next = new_node
  
  def insert_at_position(self,pos,data):
    new_node = Node(data)
    temp = self.head 
    for i in range(pos-1):
      temp = temp.next 
    new_node.data = data 
    new_node.next = temp.next
    temp.next = new_node
  
  def delete_at_begining(self):
    temp = self.head 
    self.head = temp.next 
    temp.next = None
  
  def delete_at_end(self):
    prev = self.head 
    temp = self.head.next 
    while temp.next is not None:
      temp = temp.next 
      prev = prev.next 
    prev.next = None
  
  def delete_at_position(self,pos):
    prev = self.head 
    temp = self.head.next 
    for i in range(1,pos-1):
      temp = temp.next 
      prev = prev.next 
    prev.next = temp.next
  
  def display(self):
    if self.head is None:
      print("Empty LinkedList")
    else:
      temp = self.head 
      while(temp):
        print(temp.data , end = " ")
        temp=temp.next 

l = Linkedlist()
n = Node(1)
l.head = n
n1 = Node(2)
n.next = n1 
n2 = Node(3)
n1.next = n2 
n3 = Node(4)
n2.next = n3 
l.insert_at_begining(5)
l.insert_at_end(5)
l.insert_at_position(1,5)
l.delete_at_begining()
l.delete_at_end()
l.delete_at_position(5)
l.display()